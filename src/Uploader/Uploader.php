<?php

namespace App\Uploader;

use App\Uploader\UploaderInterface;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * class Uploader
 * @package App\Uploader
 */
class Uploader implements UploaderInterface
{

    /**
     * @var SluggerInterface
     */
    
    private SluggerInterface $slugger;

    /**
     * @var string
     */
    private string $uploadsAbsoluteDir;

    /**
     * @var string
     */
    private string $uploadsRelativeDir;

    /**
     * Uploader constructor function
     *
     * @param SluggerInterface $slugger
     * @param string $uploadsAbsoluteDir
     * @param string $uploadsRelativeDir
     */
    public function __construct(SluggerInterface $slugger, string $uploadsAbsoluteDir, string $uploadsRelativeDir)
    {
        $this->slugger = $slugger;
        $this->uploadsAbsoluteDir = $uploadsAbsoluteDir;
        $this->uploadsRelativeDir = $uploadsRelativeDir;
    }
    
    /**
     * @inheritDoc
     */

    public function upload(UploadedFile $file): string
    {
        
        $filename = sprintf(
            "%s_%s.%s", 
            $this->slugger->slug($file->getClientOriginalName()),
            uniqid(),
            $file->getClientOriginalExtension()         
            );

            //sprintf est une fonction de formatage de données qui va permettre de définir un nom pour le fichier upload
            
            $file->move($this->uploadsAbsoluteDir, $filename);

            return $this->uploadsRelativeDir . "/" . $filename;
    }
}