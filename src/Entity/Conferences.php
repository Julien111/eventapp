<?php

namespace App\Entity;

use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\ConferencesRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=ConferencesRepository::class)
 */
class Conferences
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     * @Assert\Length(
     * min=3,
     * max=255,
     * minMessage = "Le nom du lieu n'est pas assez long",
     * maxMessage = "Le nom du lieu est trop long")
     * @Assert\Regex(
     *     pattern="/\d/",
     *     match=false,
     *     message="Le titre ne doit pas contenir de nombre"
     * )
     */
    private $titre;

    /**
     * @ORM\Column(type="text") 
     * @Assert\NotBlank
     * @Assert\Type("string")
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255) 
     */
    private $image;

    /**
     * @ORM\Column(type="datetime") 
     * @Assert\NotBlank
     * @Assert\Type("\DateTimeInterface") 
     */
    private $date_heure;

    /**
     * @ORM\ManyToOne(targetEntity=Users::class, inversedBy="conferences")
     * @ORM\JoinColumn(nullable=false)
     */
    private $conferenceLieAUsers;

    /**
     * @ORM\ManyToOne(targetEntity=Lieu::class, inversedBy="lieuLieConference", fetch="EAGER")
     * @ORM\JoinColumn(name="conference_liee_lieu_id", referencedColumnName="id")
     */
    private $conferenceLieeLieu;

    /**
     * @ORM\ManyToMany(targetEntity=Theme::class, inversedBy="themeLieConference", fetch="EAGER")
     * @ORM\JoinColumn(name="conference_id", referencedColumnName="id")
     */
    private $themesLiesConferences;

    /**
     * @Assert\NotBlank
     * @Assert\PositiveOrZero
     * @ORM\Column(type="decimal", precision=5, scale=2)
     */
    private $prix;

    public function __construct()
    {
        $this->themesLiesConferences = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getDateHeure(): ?\DateTimeInterface
    {
        return $this->date_heure;
    }

    public function setDateHeure(\DateTimeInterface $date_heure): self
    {
        $this->date_heure = $date_heure;

        return $this;
    }

    public function getConferenceLieAUsers(): ?Users
    {
        return $this->conferenceLieAUsers;
    }

    public function setConferenceLieAUsers(?Users $conferenceLieAUsers): self
    {
        $this->conferenceLieAUsers = $conferenceLieAUsers;

        return $this;
    }

    public function getConferenceLieeLieu(): ?Lieu
    {
        return $this->conferenceLieeLieu;
    }

    public function setConferenceLieeLieu(?Lieu $conferenceLieeLieu): self
    {
        $this->conferenceLieeLieu = $conferenceLieeLieu;

        return $this;
    }

    /**
     * @return Collection|Theme[]
     */
    public function getThemesLiesConferences(): Collection
    {
        return $this->themesLiesConferences;
    }

    public function addThemesLiesConference(Theme $themesLiesConference): self
    {
        if (!$this->themesLiesConferences->contains($themesLiesConference)) {
            $this->themesLiesConferences[] = $themesLiesConference;
            $themesLiesConference->addThemeLieConference($this);
        }

        return $this;
    }

    public function removeThemesLiesConference(Theme $themesLiesConference): self
    {
        if ($this->themesLiesConferences->removeElement($themesLiesConference)) {
            $themesLiesConference->removeThemeLieConference($this);
        }

        return $this;
    }

    public function getPrix(): ?string
    {
        return $this->prix;
    }

    public function setPrix(?string $prix): self
    {
        $this->prix = $prix;

        return $this;
    }
}