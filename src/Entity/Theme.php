<?php

namespace App\Entity;

use App\Repository\ThemeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=ThemeRepository::class)
 */
class Theme
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     * @Assert\Length(
     *  min=3,
     *  max=255,
     *  minMessage = "Le nom du thème est pas assez long",
     *  maxMessage = "Le nom du thème est trop long")
     * @Assert\NotBlank
     * @Assert\Regex(
     *     pattern="/\d/",
     *     match=false,
     *     message="Le nom du thème ne doit pas contenir de nombre"
     * )
     */
    private $nom;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank
     * @Assert\Type("string")
     */
    private $description;

    /**
     * @ORM\ManyToMany(targetEntity=Conferences::class, mappedBy="themesLiesConferences") 
     */
    private $themeLieConference;

    public function __construct()
    {
        $this->themeLieConference = new ArrayCollection();
    }

    public function __toString ()
    {
        return $this->nom;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection|Conferences[]
     */
    public function getThemeLieConference(): Collection
    {
        return $this->themeLieConference;
    }

    public function addThemeLieConference(Conferences $themeLieConference): self
    {
        if (!$this->themeLieConference->contains($themeLieConference)) {
            $this->themeLieConference[] = $themeLieConference;
        }

        return $this;
    }

    public function removeThemeLieConference(Conferences $themeLieConference): self
    {
        $this->themeLieConference->removeElement($themeLieConference);

        return $this;
    }
}