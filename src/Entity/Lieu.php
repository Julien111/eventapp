<?php

namespace App\Entity;

use App\Repository\LieuRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=LieuRepository::class)
 */
class Lieu
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(
     * min=3,
     * max=255,
     * minMessage = "Le nom du lieu n'est pas assez long",
     * maxMessage = "Le nom du lieu est trop long")
     * @Assert\Regex(
     *     pattern="/\d/",
     *     match=false,
     *     message="Votre nom ne doit pas contenir de nombre"
     * )
     * @Assert\NotBlank(message="Le champ ne peut pas être vide")
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=320)
     * @Assert\NotBlank(message="Le champ ne peut pas être vide")
     * @Assert\Type("string")
     * @Assert\Length(
     * min=10,
     * max=320,
     * minMessage = "L'adresse est trop courte.",
     * maxMessage = "L'adresse est trop longue")
     */
    private $adresse;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank(message="Le champ ne peut pas être vide")
     * @Assert\Length(
     * min=3,
     * max=100,
     * minMessage = "Le nom de la ville est trop court.",
     * maxMessage = "Le nom de la ville est trop long.")
     * @Assert\Regex(
     *     pattern="/\d/",
     *     match=false,
     *     message="Votre nom de ville ne doit pas contenir de nombre"
     * )
     */
    private $ville;

    /**
     * @ORM\Column(type="integer")
     * @Assert\Regex("/^[0-9]{5}$/", message="La valeur n'est pas valide")
     * @Assert\NotBlank(message="Le champ ne peut pas être vide")
     */
    private $code_postal;

    /**
     * @ORM\OneToMany(targetEntity=Conferences::class, mappedBy="conferenceLieeLieu", cascade={"persist", "remove"}) 
     */
    private $lieuLieConference;

    public function __construct()
    {
        $this->lieu_lie_conference = new ArrayCollection();
    }

    public function __toString ()
    {
        return $this->nom;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(string $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    public function getCodePostal(): ?int
    {
        return $this->code_postal;
    }

    public function setCodePostal(int $code_postal): self
    {
        $this->code_postal = $code_postal;

        return $this;
    }

    /**
     * @return Collection|Conferences[]
     */
    public function getLieuLieConference(): Collection
    {
        return $this->lieuLieConference;
    }

    public function addLieuLieConference(Conferences $lieuLieConference): self
    {
        if (!$this->lieuLieConference->contains($lieuLieConference)) {
            $this->lieuLieConference[] = $lieuLieConference;
            $lieuLieConference->setConferenceLieeLieu($this);
        }

        return $this;
    }

    public function removeLieuLieConference(Conferences $lieuLieConference): self
    {
        if ($this->lieu_lie_conference->removeElement($lieuLieConference)) {
            // set the owning side to null (unless already changed)
            if ($lieuLieConference->getConferenceLieeLieu() === $this) {
                $lieuLieConference->setConferenceLieeLieu(null);
            }
        }

        return $this;
    }
}