<?php

namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use App\Repository\ConferencierRepository;

/**
 * @ORM\Entity(repositoryClass=ConferencierRepository::class)
 */
class Conferencier
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\Length(
     *  min=3,
     *  max=100,
     *  minMessage = "Votre nom est pas assez long",
     *  maxMessage = "Votre nom est trop long")
     * @Assert\NotBlank
     * @Assert\Regex(
     *     pattern="/\d/",
     *     match=false,
     *     message="Votre nom ne doit pas contenir de nombre"
     * )
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\Regex(
     *     pattern="/\d/",
     *     match=false,
     *     message="Votre prénom ne doit pas contenir de nombre"
     * )
     * @Assert\NotBlank 
     * @Assert\Length(
     * min=3,
     * max=100,
     * minMessage = "Votre prénom est pas assez long",
     * maxMessage = "Votre prénom est trop long")
     */
    private $prenom;

    /**
     * @ORM\Column(type="date")
     * @Assert\NotBlank
     * @Assert\Type("\DateTimeInterface")
     */
    private $date_naissance;

    /**
     * @ORM\OneToOne(targetEntity=Users::class, inversedBy="conferencier")
     * @ORM\JoinColumn(nullable=false) 
     */
    private $conferencierLieUsers;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getDateNaissance(): ?\DateTimeInterface
    {
        return $this->date_naissance;
    }

    public function setDateNaissance(\DateTimeInterface $date_naissance): self
    {
        $this->date_naissance = $date_naissance;

        return $this;
    }

    public function getConferencierLieUsers(): ?Users
    {
        return $this->conferencierLieUsers;
    }

    public function setConferencierLieUsers(Users $conferencierLieUsers): self
    {
        $this->conferencierLieUsers = $conferencierLieUsers;

        return $this;
    }
}