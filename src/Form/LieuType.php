<?php

namespace App\Form;

use App\Entity\Lieu;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class LieuType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom',TextType::class, [
            'required' => true,
            'constraints' => [new Length(['min' => 3, 'max' => 255])],
        ])
            ->add('adresse',TextType::class, [
            'required' => true,
            'constraints' => [new Length(['min' => 3, 'max' => 320])],
        ])
            ->add('ville',TextType::class, [
            'required' => true,
            'constraints' => [new Length(['min' => 3, 'max' => 100])],
        ])
            ->add('code_postal')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Lieu::class,
        ]);
    }
}