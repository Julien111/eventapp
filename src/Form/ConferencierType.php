<?php

namespace App\Form;

use App\Entity\Conferencier;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;

class ConferencierType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom',TextType::class, [
            'required' => true,
            'constraints' => [ new NotBlank([
                        'message' => 'Entrez votre nom']), new Length(['min' => 3, 'max' => 100]), 
                    new Regex("/^[a-z ,.'-]+$/i")],
        ])
            ->add('prenom', TextType::class, [
            'required' => true,
            'constraints' => [ new NotBlank([
                        'message' => 'Entrez votre prénom']), new Length(['min' => 3, 'max' => 100]), 
                    new Regex("/^[a-z ,.'-]+$/i")],
        ])
            ->add('date_naissance', BirthdayType::class, [
                'placeholder' => [
                'year' => 'Année', 'month' => 'Mois', 'day' => 'Jour',
                ],
                'required' => true,
                'constraints' => [new NotBlank([
                        'message' => 'Entrez votre date de naissance'])]
            ])            
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Conferencier::class,
        ]);
    }
}