<?php

namespace App\Form;

use App\Entity\Users;
use App\Entity\Conferencier;
use App\Form\ConferencierType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, [
                'constraints' => [ new NotBlank([
                    'message' => 'Merci de saisir une adresse email'
                ])],
                'required' =>true,
            ])
            ->add('agreeTerms', CheckboxType::class, [
                'mapped' => false,
                'constraints' => [
                    new IsTrue([
                        'message' => 'You should agree to our terms.',
                    ]),
                ],
            ])
            ->add('plainPassword', RepeatedType::class, [
                "type" => PasswordType::class,
                'mapped' => false,
                'first_options' => [
                    "label" => 'Mot de passe :'
                ],
                'second_options' => [
                    'label' => "Confirmez votre mot de passe :"
                ],
                'invalid_message' => "La confirmation n'est pas similaire au mot de passe.",
                'constraints' => [
                    new NotBlank(),
                    new Length(["min" => 8]),
                ]
            ])
            ->add('conferencier', ConferencierType::class, ["label" => false])
            ;            
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Users::class,            
        ]);
    }
}