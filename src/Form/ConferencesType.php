<?php

namespace App\Form;

use App\Entity\Lieu;
use App\Entity\Theme;
use App\Entity\Conferences;
use App\Entity\Conferencier;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class ConferencesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titre', TextType::class, ["required" => true])
            ->add('description', TextareaType::class, ["required" => true, "attr" => ['id' => 'msg']])
            ->add("file", FileType::class, [
                "required" => true,
                "mapped" =>false,
                "constraints" => [
                    new Image(),
                    new NotNull([
                        "groups" => "create"
                    ])
                ]
            ])
            ->add('date_heure', DateTimeType::class, [
            'date_label' => 'Commence le : ',            
            "required" => true,
            'years' => range(date('Y'), date('Y') + 8)])                        
            ->add('conferenceLieeLieu', EntityType::class, ["class" => Lieu::class, "label" => "Lieu : "])
            ->add('themesLiesConferences', EntityType::class, ["class" => Theme::class, "label" => "Thème : ", "attr" => ["class" => "check"], 'multiple' => true,
                'expanded' => true,])
            ->add('prix', null, ['html5' => true, 'scale' => 2])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Conferences::class,
        ]);
    }
}