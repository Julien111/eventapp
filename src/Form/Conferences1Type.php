<?php

namespace App\Form;

use App\Entity\Lieu;
use App\Entity\Theme;
use App\Entity\Users;
use App\Entity\Conferences;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class Conferences1Type extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titre', TextType::class, ["required" => true])
            ->add('description', TextareaType::class, ["required" => true])
            ->add("file", FileType::class, [
                "required" => true,
                "mapped" =>false,
                "constraints" => [
                    new Image(),
                    new NotNull([
                        "groups" => "create"
                    ])
                ]
            ])
            ->add('date_heure', DateTimeType::class, [
            'date_label' => 'Commence le : ',
            "required" => true])            
            ->add('conferenceLieeLieu', EntityType::class, ["label" => "Lieu : ", "class" => Lieu::class])
            ->add('themesLiesConferences', EntityType::class, ["class" => Theme::class, "label" => "Thème : ", 'multiple' => true,
                'expanded' => true,])
            ->add('prix', null, ['html5' => true, 'scale' => 2])
            ->add('conferenceLieAUsers', EntityType::class, ["label" => "Email user : ", "disabled" => true, "class" => Users::class])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Conferences::class,
        ]);
    }
}