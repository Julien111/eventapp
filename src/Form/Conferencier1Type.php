<?php

namespace App\Form;

use App\Entity\Conferencier;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;

class Conferencier1Type extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom',TextType::class, [
            'required' => true,
            'constraints' => [new Length(['min' => 3])],
        ])
            ->add('prenom', TextType::class, [
            'required' => true,
            'constraints' => [new Length(['min' => 3])],
        ])
            ->add('date_naissance', BirthdayType::class, [
                'required' => true,
            ])            
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Conferencier::class,
        ]);
    }
}