<?php

namespace App\Repository;

use App\Entity\Conferencier;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Conferencier|null find($id, $lockMode = null, $lockVersion = null)
 * @method Conferencier|null findOneBy(array $criteria, array $orderBy = null)
 * @method Conferencier[]    findAll()
 * @method Conferencier[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ConferencierRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Conferencier::class);
    }

    // /**
    //  * @return Conferencier[] Returns an array of Conferencier objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Conferencier
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
