<?php

namespace App\Repository;

use DateTime;
use App\Entity\Conferences;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method Conferences|null find($id, $lockMode = null, $lockVersion = null)
 * @method Conferences|null findOneBy(array $criteria, array $orderBy = null)
 * @method Conferences[]    findAll()
 * @method Conferences[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ConferencesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Conferences::class);
    }


    /**
     * Classer des éléments par date.
     * @return Event[] Returns an array of upcoming Event objects
     */
    public function findConferences() 
    {
        return $this->createQueryBuilder('conferences')
        ->andWhere('conferences.date_heure > :now')
        ->setParameter(':now', new \DateTime)
        ->orderBy('conferences.date_heure', 'ASC')
        ->getQuery()
        ->getResult();
    }

    
    // /**
    //  * @return Conferences[] Returns an array of Conferences objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Conferences
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}