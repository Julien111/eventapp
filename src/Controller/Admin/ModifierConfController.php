<?php

namespace App\Controller\Admin;

use App\Entity\Conferences;
use App\Form\Conferences1Type;
use App\Uploader\UploaderInterface;
use App\Repository\ConferencesRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @IsGranted("ROLE_ADMIN")
 * @Route("/admin/modifier/conf")
 */
class ModifierConfController extends AbstractController
{
    /**
     * @IsGranted("ROLE_ADMIN")
     * @Route("/", name="modifier_conf_index", methods={"GET"})
     */
    public function index(ConferencesRepository $conferencesRepository): Response
    {
        return $this->render('admin/modifier_conf/index.html.twig', [
            'conferences' => $conferencesRepository->findAll(),
        ]);
    }

    /**
     * @IsGranted("ROLE_ADMIN")
     * @Route("/{id}", name="modifier_conf_show", methods={"GET"})
     */
    public function show(Conferences $conference): Response
    {
        return $this->render('admin/modifier_conf/show.html.twig', [
            'conference' => $conference,
        ]);
    }

    /**
     * @IsGranted("ROLE_ADMIN")
     * @Route("/{id}/edit", name="modifier_conf_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Conferences $conference,  UploaderInterface $uploader): Response
    {

        $form = $this->createForm(Conferences1Type::class, $conference);
        $form->handleRequest($request);

        
        if ($form->isSubmitted() && $form->isValid()) {

            $images = $conference->getImage();

            $imagesFinale = substr($images, 7);

            //dd($imagesFinale);

            $nomImage = $this->getParameter("uploadsAbsoluteDir") . $imagesFinale;

            if(file_exists($nomImage)){
                unlink($nomImage);
            }

            $file = $form->get("file")->getData();

            //Ici on récupère le fichier par le get et on envoit une (image)

            if($file !== null){
                $conference->setImage($uploader->upload($file));                

            }
            $conference->setConferenceLieAUsers($this->getUser());

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute("modifier_conf_index");
        }

        return $this->render('admin/modifier_conf/edit.html.twig', [
            'conference' => $conference,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @IsGranted("ROLE_ADMIN")
     * @Route("/{id}", name="modifier_conf_delete", methods={"POST"})
     */
    public function delete(Request $request, Conferences $conference): Response
    {           
            
        if ($this->isCsrfTokenValid('delete'.$conference->getId(), $request->request->get('_token'))) {

            $images = $conference->getImage();

            $imagesFinale = substr($images, 7);

            //dd($imagesFinale);

            $nomImage = $this->getParameter("uploadsAbsoluteDir") . $imagesFinale;

            if(file_exists($nomImage)){
                unlink($nomImage);
            
            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($conference);
            $entityManager->flush();
        }

        return $this->redirectToRoute('modifier_conf_index');
    }
}