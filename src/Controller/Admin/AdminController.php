<?php

namespace App\Controller\Admin;

use App\Entity\Theme;
use App\Entity\Users;
use App\Form\ThemeType;
use App\Entity\Conferences;
use App\Repository\ThemeRepository;
use App\Repository\UsersRepository;
use App\Repository\ConferencesRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
* @IsGranted("ROLE_ADMIN")
* @Route("/admin")
* @package App\Controller\Admin
*/
class AdminController extends AbstractController
{
    /**
     * @IsGranted("ROLE_ADMIN")
     * @Route("/", name="admin_accueil")
     */
    public function index(): Response
    {
        return $this->render('admin/index.html.twig', [
            'controller_name' => 'AdminController',
        ]);
    }

    /**
     * @IsGranted("ROLE_ADMIN")
     * @Route("/theme", name="theme_index", methods={"GET","POST"})
     */
    public function themeIndex(ThemeRepository $repo): Response
    {
        return $this->render('admin/theme/index.html.twig', [
        'theme' => $repo->findAll(),
        ]);
    }

    /**
     * @IsGranted("ROLE_ADMIN")
     * @Route("/theme/new", name="theme_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $theme = new Theme();
        $form = $this->createForm(ThemeType::class, $theme);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($theme);
            $entityManager->flush();

            return $this->redirectToRoute('theme_index');
        }

        return $this->render('admin/theme/theme.html.twig', [
            'theme' => $theme,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @IsGranted("ROLE_ADMIN")
     * @Route("/theme/{id}/edit", name="theme_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Theme $theme): Response
    {
        $form = $this->createForm(ThemeType::class, $theme);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('theme_index');
        }

        return $this->render('admin/theme/edit.html.twig', [
            'theme' => $theme,
            'form' => $form->createView(),
        ]);
    }

    /**
    * @IsGranted("ROLE_ADMIN")
    * @Route("/utilisateurs", name="utilisateurs")
    */
    public function usersList(UsersRepository $users)
    {
    return $this->render('admin/utilisateurs/users.html.twig', [
        'users' => $users->findAll(),
    ]);
    }

    /**
     * @IsGranted("ROLE_ADMIN")
     * @Route("/utilisateurs/{id}", name="supprimer_user", methods={"GET","POST"})
     */
    public function delete(Request $request, Users $users, ConferencesRepository $conferencesRepository): Response
    {
        $id_user = $users->getId();

        //dd($id_user);
        $result = $conferencesRepository->findBy([
                "conferenceLieAUsers" => $id_user,
            ]);

        $images = [];
        
        foreach($result as $key => $value){

            array_push($images, $value->getImage());       
        
        }
        
        for($i = 0; $i < count($images); $i++){
            
            $img = $images[$i];
            $imagesFinale = substr($img, 7);

            $nomImage = $this->getParameter("uploadsAbsoluteDir") . $imagesFinale;

        
            if(file_exists($nomImage)){
                unlink($nomImage);
            }
            
        } 
        
        
        if ($this->isCsrfTokenValid('delete'.$users->getId(), $request->request->get('_token'))) {

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($users);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin_accueil');
    }      
    
}