<?php

namespace App\Controller\Profil;

use App\Entity\Users;
use App\Service\Securizer;
use App\Entity\Conferencier;
use App\Form\Conferencier1Type;
use App\Repository\ConferencesRepository;
use App\Repository\ConferencierRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @IsGranted("ROLE_USER")
 * @Route("/profil/conferencier")
 */
class ProfilConferencierController extends AbstractController
{
    /**
     * @IsGranted("ROLE_USER")
     * @Route("/", name="profil_conferencier_index", methods={"GET"})
     */
    public function index(ConferencierRepository $conferencierRepository,  Securizer $securizer): Response
    {
        if($securizer->isGranted($this->getUser(), 'ROLE_ADMIN')){
            return $this->redirectToRoute('admin_accueil');
        }
        
        //dd($conferencierRepository);
        return $this->render('profil/profil_conferencier/index.html.twig', [
            'conferencier' => $conferencierRepository->find($this->getUser())           
        ]);
    }    

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/{id}", name="profil_conferencier_show", methods={"GET"})
     */
    public function show(Conferencier $conferencier, Securizer $securizer): Response
    {
        if($securizer->isGranted($this->getUser(), 'ROLE_ADMIN')){
            return $this->redirectToRoute('admin_accueil');
        }

        return $this->render('profil/profil_conferencier/show.html.twig', [
            'conferencier' => $conferencier,
        ]);
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/{id}/edit", name="profil_conferencier_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Conferencier $conferencier,  Securizer $securizer): Response
    {
        if($securizer->isGranted($this->getUser(), 'ROLE_ADMIN')){
            return $this->redirectToRoute('admin_accueil');
        }
        
        $form = $this->createForm(Conferencier1Type::class, $conferencier);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('profil_conferencier_index');
        }

        return $this->render('profil/profil_conferencier/edit.html.twig', [
            'conferencier' => $conferencier,
            'form' => $form->createView(),
        ]);
    }   

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/supprimer/{id}", name="supprimer_compte", methods={"GET","POST"})
     */
    public function delete(Request $request, Users $users, ConferencesRepository $conferencesRepository): Response
    {
        $id_user = $users->getId();

        
        $result = $conferencesRepository->findBy([
                "conferenceLieAUsers" => $id_user,
            ]);

        $images = [];
        
        foreach($result as $key => $value){

            array_push($images, $value->getImage());       
        
        }
        
        for($i = 0; $i < count($images); $i++){
            
            $img = $images[$i];
            $imagesFinale = substr($img, 7);

            $nomImage = $this->getParameter("uploadsAbsoluteDir") . $imagesFinale;

        
            if(file_exists($nomImage)){
                unlink($nomImage);
            }
            
        } 
        
        
        if ($this->isCsrfTokenValid('delete'.$users->getId(), $request->request->get('_token'))) {

            $this->container->get('security.token_storage')->setToken(null);

            $entityManager = $this->getDoctrine()->getManager();            
            $entityManager->remove($users);
            $entityManager->flush();
        }
        $this->addFlash('success', 'Votre compte utilisateur a bien été supprimé !'); 

        return $this->redirectToRoute('accueil');
    }
}