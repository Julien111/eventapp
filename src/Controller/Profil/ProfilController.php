<?php

namespace App\Controller\Profil;

use App\Entity\Users;
use App\Form\UsersType;
use App\Service\Securizer;
use App\Entity\Conferences;
use App\Form\ConferencesType;
use App\Repository\UsersRepository;
use App\Uploader\UploaderInterface;
use App\Repository\ConferencesRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @IsGranted("ROLE_USER")
 * @Route("/profil")
 * @package App\Controller\Profil
 */
class ProfilController extends AbstractController
{

    /**
     * @IsGranted("ROLE_USER")     
     * @Route("/", name="profil_index", methods={"GET"})
     */
    public function profilIndex(Securizer $securizer): Response
    {   
        
        //gestion des rôles

        if($securizer->isGranted($this->getUser(), 'ROLE_ADMIN')){
            return $this->redirectToRoute('admin_accueil');
        }

        return $this->render('profil/accueil.html.twig');
    }
    
    /**
     * @IsGranted("ROLE_USER")     
     * @Route("/conferences", name="profil_conferences", methods={"GET"})
     */
    public function index(ConferencesRepository $conferencesRepository, Securizer $securizer): Response
    {   
        
        //gestion des rôles

        if($securizer->isGranted($this->getUser(), 'ROLE_ADMIN')){
            return $this->redirectToRoute('admin_accueil');
        }

        return $this->render('profil/index.html.twig', [
            'conferences' => $conferencesRepository->findBy([
                "conferenceLieAUsers" => $this->getUser(),
            ]),
        ]);
    }


    /**
     * @IsGranted("ROLE_USER")
     * @Route("/informations", name="modifier_vos_informations", methods={"GET", "POST"})
     */
    public function profileEdit(Request $request, UsersRepository $user, Securizer $securizer, UserPasswordEncoderInterface $passwordEncoder) : Response
    {
        //gestion des rôles
            
        if($securizer->isGranted($this->getUser(), 'ROLE_ADMIN')){
            return $this->redirectToRoute('admin_accueil');
        }
        
        $user = $this->getUser();

        $form = $this->createForm(UsersType::class, $user);
        $form->handleRequest($request);

        
        if ($form->isSubmitted() && $form->isValid()) {

            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            $this->addFlash('msg', 'Vous informations ont bien été modifiées !');
            
            return $this->redirectToRoute('profil_index');
        }

        return $this->render('profil/profilEdit.html.twig', [
            'user'=>$user,           
            'form'=> $form->createView(),
        ]);
    }
    
    /**
     * @IsGranted("ROLE_USER")
     * @Route("/new", name="creer_conference", methods={"GET","POST"})
     */
    public function new(Request $request, UploaderInterface $uploader, Securizer $securizer): Response
    {

        //gestion des rôles
        
        if($securizer->isGranted($this->getUser(), 'ROLE_ADMIN')){
            return $this->redirectToRoute('admin_accueil');
        }
        
        $conference = new Conferences();
        $form = $this->createForm(ConferencesType::class, $conference);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $file = $form->get("file")->getData();
            $conference->setImage($uploader->upload($file));

            $conference->setConferenceLieAUsers($this->getUser());
            
            
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($conference);
            $entityManager->flush();

            return $this->redirectToRoute('profil_conferences');
        }

        return $this->render('profil/new.html.twig', [
            'conference' => $conference,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/{id}", name="montrer_conference", methods={"GET"})
     */
    public function show(Conferences $conference, Securizer $securizer): Response
    {   

        
        //gestion des rôles
        
        if($securizer->isGranted($this->getUser(), 'ROLE_ADMIN')){
            return $this->redirectToRoute('admin_accueil');
        }


        return $this->render('profil/show.html.twig', [
            'conference' => $conference,
        ]);
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/{id}/modifier_conference", name="modifier_conference", methods={"GET","POST"})
     */
    public function edit(Request $request, Conferences $conference,  UploaderInterface $uploader, Securizer $securizer): Response
    {

        //gestion des rôles
        
        if($securizer->isGranted($this->getUser(), 'ROLE_ADMIN')){
            return $this->redirectToRoute('admin_accueil');
        }


        $form = $this->createForm(ConferencesType::class, $conference);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $images = $conference->getImage();

            $imagesFinale = substr($images, 7);

            //dd($imagesFinale);

            $nomImage = $this->getParameter("uploadsAbsoluteDir") . $imagesFinale;

            if(file_exists($nomImage)){
                unlink($nomImage);
            }

            $file = $form->get("file")->getData();

            //Ici on récupère le fichier par le get et on envoit une (image)

            if($file !== null){
                $conference->setImage($uploader->upload($file));                

            }
            $conference->setConferenceLieAUsers($this->getUser());

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('profil_conferences');
        }

        return $this->render('profil/edit.html.twig', [
            'conference' => $conference,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/{id}", name="supprimer_conference", methods={"POST"})
     */
    public function delete(Request $request, Conferences $conference, Securizer $securizer): Response
    {

        //gestion des rôles
            
        if($securizer->isGranted($this->getUser(), 'ROLE_ADMIN')){
            return $this->redirectToRoute('admin_accueil');
        }


        if ($this->isCsrfTokenValid('delete'.$conference->getId(), $request->request->get('_token'))) {

            $images = $conference->getImage();

            $imagesFinale = substr($images, 7);

            //dd($imagesFinale);

            $nomImage = $this->getParameter("uploadsAbsoluteDir") . $imagesFinale;

            if(file_exists($nomImage)){
                unlink($nomImage);
            }
            
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($conference);
            $entityManager->flush();
        }

        return $this->redirectToRoute('profil_conferences');
    }    
}