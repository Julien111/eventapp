<?php

namespace App\Controller;

use App\Entity\Theme;
use App\Form\ContactType;
use App\Entity\Conferences;
use App\Repository\ThemeRepository;
use App\Repository\ConferencesRepository;
use App\Repository\ConferencierRepository;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class AccueilController extends AbstractController
{
    /**
     * @Route("/", name="accueil")
     */
    public function index(ConferencesRepository $conferencesRepository, ThemeRepository $themeRepository): Response
    {   
        $conferences = $conferencesRepository->findConferences();  
        $themes = $themeRepository->findAll();  
    
        return $this->render('accueil/index.html.twig', [
            'conferences' => $conferences,
            'themes' => $themes
        ]);
    }
    /**
     * @Route("/conference-{id}", name="conference_read")
     * @param Conferences $conferences
     * @param Theme $themes
     * @return Response
     */
    public function read(Conferences $conferences, ThemeRepository $themeRepository, ConferencierRepository $repoConferencier): Response
    {
        $themes = $themeRepository->findAll();
        $conferenciers = $repoConferencier->findAll();
        
        
        return $this->render("accueil/read.html.twig", [
            "conference" => $conferences, 
            "conferenciers" => $conferenciers, 
            'themes' => $themes         
        ]);
    }

    /**
     * @Route("/theme/{nom}", name="conference_theme")
     * @return Response
     */
    public function conferenceByTheme(string $nom, ThemeRepository $themeRepository): Response
    {
        $themes = $themeRepository->findAll();
        
        $result = $themeRepository->findBy(array('nom' => $nom));
    
    
        return $this->render("accueil/themeConference.html.twig", [
            'themes' => $themes,         
            'conf' => $result         
        ]);
    }


    /**
    * @Route("/about", name="page_a_propos")
    */
    public function pageAPropos(ThemeRepository $themeRepository): Response
    {
        $themes = $themeRepository->findAll();
        return $this->render('accueil/about.html.twig', [
            'themes' => $themes, 
        ]);
    }

    /**
    * @Route("/contact", name="contact")
    */
    public function pageContact(Request $request, MailerInterface $mailer, ThemeRepository $themeRepository): Response
    {

        $themes = $themeRepository->findAll();

        $form = $this->createForm(ContactType::class);

        $contact = $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $mail = "julien.farrechavanon@gmail.com";

            //création du mail
            $email = ( new TemplatedEmail())
                ->from($contact->get('email')->getData())
                ->to($mail)
                ->subject('Message depuis le site Event App')
                ->htmlTemplate('emails/contact_visiteur.html.twig')
                ->context([
                    'sujet' => $contact->get('sujet')->getData(),
                    'nom' => $contact->get('nom')->getData(),
                    'mail' => $contact->get('email')->getData(),
                    'message' => $contact->get('message')->getData(),
                ]);
                //On envoie le mail

                
                $mailer->send($email);

                $this->addFlash('message', 'Votre message a bien été envoyé !');

                return $this->redirectToRoute('contact');
        }

        return $this->render('accueil/contact.html.twig', [
            'form' => $form->createView(),
            'themes' => $themes,
        ]);
    }
}