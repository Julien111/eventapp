<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210413201135 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE conferences DROP FOREIGN KEY FK_8E2090BA2A74AF29');
        $this->addSql('DROP INDEX IDX_8E2090BA2A74AF29 ON conferences');
        $this->addSql('ALTER TABLE conferences CHANGE conference_lie_a_users_id conference_lie_ausers_id INT NOT NULL');
        $this->addSql('ALTER TABLE conferences ADD CONSTRAINT FK_8E2090BA66811920 FOREIGN KEY (conference_lie_ausers_id) REFERENCES users (id)');
        $this->addSql('CREATE INDEX IDX_8E2090BA66811920 ON conferences (conference_lie_ausers_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE conferences DROP FOREIGN KEY FK_8E2090BA66811920');
        $this->addSql('DROP INDEX IDX_8E2090BA66811920 ON conferences');
        $this->addSql('ALTER TABLE conferences CHANGE conference_lie_ausers_id conference_lie_a_users_id INT NOT NULL');
        $this->addSql('ALTER TABLE conferences ADD CONSTRAINT FK_8E2090BA2A74AF29 FOREIGN KEY (conference_lie_a_users_id) REFERENCES users (id)');
        $this->addSql('CREATE INDEX IDX_8E2090BA2A74AF29 ON conferences (conference_lie_a_users_id)');
    }
}
