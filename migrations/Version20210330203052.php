<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210330203052 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE users DROP FOREIGN KEY FK_1483A5E983257E3B');
        $this->addSql('DROP INDEX UNIQ_1483A5E983257E3B ON users');
        $this->addSql('ALTER TABLE users DROP user_lie_conferencier_id');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE users ADD user_lie_conferencier_id INT NOT NULL');
        $this->addSql('ALTER TABLE users ADD CONSTRAINT FK_1483A5E983257E3B FOREIGN KEY (user_lie_conferencier_id) REFERENCES conferencier (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1483A5E983257E3B ON users (user_lie_conferencier_id)');
    }
}
