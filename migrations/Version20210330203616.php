<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210330203616 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE conferencier ADD conferencier_lie_users_id INT NOT NULL');
        $this->addSql('ALTER TABLE conferencier ADD CONSTRAINT FK_52B1C92C78D6E08C FOREIGN KEY (conferencier_lie_users_id) REFERENCES users (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_52B1C92C78D6E08C ON conferencier (conferencier_lie_users_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE conferencier DROP FOREIGN KEY FK_52B1C92C78D6E08C');
        $this->addSql('DROP INDEX UNIQ_52B1C92C78D6E08C ON conferencier');
        $this->addSql('ALTER TABLE conferencier DROP conferencier_lie_users_id');
    }
}
