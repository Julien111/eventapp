<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210401103226 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE theme_conferences (theme_id INT NOT NULL, conferences_id INT NOT NULL, INDEX IDX_660B83B559027487 (theme_id), INDEX IDX_660B83B5E4A714AA (conferences_id), PRIMARY KEY(theme_id, conferences_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE theme_conferences ADD CONSTRAINT FK_660B83B559027487 FOREIGN KEY (theme_id) REFERENCES theme (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE theme_conferences ADD CONSTRAINT FK_660B83B5E4A714AA FOREIGN KEY (conferences_id) REFERENCES conferences (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE conferences DROP FOREIGN KEY FK_8E2090BA2A74AF29');
        $this->addSql('ALTER TABLE conferences ADD CONSTRAINT FK_8E2090BA2A74AF29 FOREIGN KEY (conference_lie_a_users_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE conferencier DROP FOREIGN KEY FK_52B1C92C78D6E08C');
        $this->addSql('ALTER TABLE conferencier ADD CONSTRAINT FK_52B1C92C78D6E08C FOREIGN KEY (conferencier_lie_users_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE users ADD conferencier_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE users ADD CONSTRAINT FK_1483A5E9F14895FB FOREIGN KEY (conferencier_id) REFERENCES conferencier (id) ON DELETE CASCADE');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1483A5E9F14895FB ON users (conferencier_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE theme_conferences');
        $this->addSql('ALTER TABLE conferences DROP FOREIGN KEY FK_8E2090BA2A74AF29');
        $this->addSql('ALTER TABLE conferences ADD CONSTRAINT FK_8E2090BA2A74AF29 FOREIGN KEY (conference_lie_a_users_id) REFERENCES users (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE conferencier DROP FOREIGN KEY FK_52B1C92C78D6E08C');
        $this->addSql('ALTER TABLE conferencier ADD CONSTRAINT FK_52B1C92C78D6E08C FOREIGN KEY (conferencier_lie_users_id) REFERENCES users (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE users DROP FOREIGN KEY FK_1483A5E9F14895FB');
        $this->addSql('DROP INDEX UNIQ_1483A5E9F14895FB ON users');
        $this->addSql('ALTER TABLE users DROP conferencier_id');
    }
}
