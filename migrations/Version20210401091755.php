<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210401091755 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE conferences DROP FOREIGN KEY FK_8E2090BA2A74AF29');
        $this->addSql('ALTER TABLE conferences DROP categorie');
        $this->addSql('ALTER TABLE conferences ADD CONSTRAINT FK_8E2090BA2A74AF29 FOREIGN KEY (conference_lie_a_users_id) REFERENCES users (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE conferencier DROP FOREIGN KEY FK_52B1C92C78D6E08C');
        $this->addSql('ALTER TABLE conferencier ADD CONSTRAINT FK_52B1C92C78D6E08C FOREIGN KEY (conferencier_lie_users_id) REFERENCES users (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE users ADD conferencier_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE users ADD CONSTRAINT FK_1483A5E9F14895FB FOREIGN KEY (conferencier_id) REFERENCES conferencier (id) ON DELETE CASCADE');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1483A5E9F14895FB ON users (conferencier_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE conferences DROP FOREIGN KEY FK_8E2090BA2A74AF29');
        $this->addSql('ALTER TABLE conferences ADD categorie VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE conferences ADD CONSTRAINT FK_8E2090BA2A74AF29 FOREIGN KEY (conference_lie_a_users_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE conferencier DROP FOREIGN KEY FK_52B1C92C78D6E08C');
        $this->addSql('ALTER TABLE conferencier ADD CONSTRAINT FK_52B1C92C78D6E08C FOREIGN KEY (conferencier_lie_users_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE users DROP FOREIGN KEY FK_1483A5E9F14895FB');
        $this->addSql('DROP INDEX UNIQ_1483A5E9F14895FB ON users');
        $this->addSql('ALTER TABLE users DROP conferencier_id');
    }
}
