<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210331102737 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE conferences ADD conference_liee_lieu_id INT NOT NULL');
        $this->addSql('ALTER TABLE conferences ADD CONSTRAINT FK_8E2090BAA836038E FOREIGN KEY (conference_liee_lieu_id) REFERENCES lieu (id)');
        $this->addSql('CREATE INDEX IDX_8E2090BAA836038E ON conferences (conference_liee_lieu_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE conferences DROP FOREIGN KEY FK_8E2090BAA836038E');
        $this->addSql('DROP INDEX IDX_8E2090BAA836038E ON conferences');
        $this->addSql('ALTER TABLE conferences DROP conference_liee_lieu_id');
    }
}
