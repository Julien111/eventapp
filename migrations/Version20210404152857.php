<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210404152857 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE conferences_theme (conferences_id INT NOT NULL, theme_id INT NOT NULL, INDEX IDX_3C838523E4A714AA (conferences_id), INDEX IDX_3C83852359027487 (theme_id), PRIMARY KEY(conferences_id, theme_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE conferences_theme ADD CONSTRAINT FK_3C838523E4A714AA FOREIGN KEY (conferences_id) REFERENCES conferences (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE conferences_theme ADD CONSTRAINT FK_3C83852359027487 FOREIGN KEY (theme_id) REFERENCES theme (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE theme_conferences');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE theme_conferences (theme_id INT NOT NULL, conferences_id INT NOT NULL, INDEX IDX_660B83B559027487 (theme_id), INDEX IDX_660B83B5E4A714AA (conferences_id), PRIMARY KEY(theme_id, conferences_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE theme_conferences ADD CONSTRAINT FK_660B83B559027487 FOREIGN KEY (theme_id) REFERENCES theme (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE theme_conferences ADD CONSTRAINT FK_660B83B5E4A714AA FOREIGN KEY (conferences_id) REFERENCES conferences (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE conferences_theme');
    }
}
